#!/usr/local/bin/python
import os


from flask import Flask
app = Flask(__name__)


def wrap_html(message):
    html = """
        <html>
        <body>
            <div style='font-size:120px;'>
            <center>
                <image height="200" width="800" src="https://www.cquest-rc.com/images/logo.png"> 
                <br>
                {0}<br>
            </center>
            </div>
        </body>
        </html>""".format(message)
    return html


@app.route("/")
def hello():
    msg = "Hello World CQuest !"
    html = wrap_html(msg)
    return html
 

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')

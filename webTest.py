
import webApp
import unittest


class testWebApp(unittest.TestCase):

    def setUp(self):
        self.app = webApp.app.test_client()
        self.app.testing = True

    def test_status_code(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_message(self):
        response = self.app.get('/')

        message = b"Hello World CQuest !"
        #message = webApp.wrap_html('Hello World CQuest')
        assert message in response.data

if __name__ == '__main__':
    unittest.main()

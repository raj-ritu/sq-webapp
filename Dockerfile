FROM python
RUN mkdir -p /app
ADD . /app
WORKDIR /app
RUN ls
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["webApp.py"]
